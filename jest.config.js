module.exports = {
    // transform: { '.+\\.(js|jsx|ts|tsx|svg)$': 'ts-jest' },
    moduleNameMapper: { '\\.(css|less)$': '<rootDir>/styleMock.js' },
    setupFiles: ['<rootDir>/jest.setup.js'],
    transformIgnorePatterns: ['<rootDir>/node_modules/'],
    // modulePaths: [
    //     '<rootDir>/'
    // ]
};