import axios from 'axios';
import unsplash from '../unsplash';

jest.mock('axios');

test('call axios and return result', async () => {

    const response = {
        data: {
            result: ['cute.jpg']
        }
    };
    axios.get.mockImplementation(() => Promise.resolve(response));
    
    //work
    const image = await unsplash('kitten');

    //assertions expects
    expect(image).toEqual(['cute.jpg']);
    expect(axios.get).toHaveBeenCalledTimes(1);
    expect(axios.get).toHaveBeenCalledWith("https//api.unsplash.com", {"params": {"query": "kitten"}});
    
})