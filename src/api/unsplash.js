import axios from 'axios';

const unsplash =  async term => {
    const response = await axios.get('https//api.unsplash.com', {
        params: {
            query: term
        }
    })

    return response.data.result;
};

export default unsplash;